# drone_board

Бортовая часть


Этот репозиторий https://bitbucket.org/nee77/drone_board/
Сервер: https://bitbucket.org/nee77/drone_server/
Станция: https://bitbucket.org/nee77/drone_station/



# Установка

## Node JS

    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt-get install -y nodejs build-essential

## Менеджер процессов PM2

    sudo npm install pm2 -g


## drone_board

    git clone https://nee77@bitbucket.org/nee77/drone_board.git
    cd drone_board
    npm install

Конфигурация

    nano config.js



## mavproxy.sh

запускает скрипт MAVProxy с ретрансляцией пакетов в TCP для GCS и локальный UPD порт для скрипта
телеметрия транслируется на сервер через socket.io

    pm2 start mavproxy.sh --name mavproxy

Старт скрипта drone.js

    pm2 start drone.js





