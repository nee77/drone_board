/*

1. принимает mavlink сообщения на локальный UDP порт, формирует данные в json и отправляет на сервер посредством socket.io
    в постоянном режиме отправляется минимальный набор данных telemetry1 в канал telem1
    по команде с сервера может отправлять
        - полный набор telemetry2 в канал telem2
        - чистый mavlink в канал mav
2. получает команды с сервера и реагирует на них
    telem2_start
    telem2_stop
    raw_mav_start
    raw_mav_stop

    takeoff


 */

const config = require('./config');

const CommandControllers = require('./controllers/commands');

// Инициализация UDP сервера
const dgram = require('dgram');
const udp_server = dgram.createSocket('udp4');

// Node-mavlink
const MAVlink = require('mavlink');

// Инициализация MAVLink
const drone_mavlink = new MAVlink(config.BOARD_SYS_ID, config.BOARD_COMP_ID, config.MAVLINK_VERSION, config.MAVLINK_MSG_DEF);

// Инициализация клиента socket.io
const io = require('socket.io-client');
// Соединение с socket.io сервером
const socket = io.connect(config.MAV_IO_SERVER + '?me=drone&drone_id=' + config.DRONE_ID);


let io_connected = false;
let mavlink_ready = false;
let transmit_raw_mavlink = false;
let transmit_telem2 = false;

let send_interval = null;
let send_interval2 = null;

// Накопитель исходящих сообщений, которые нужно отправить после приема сообщения по UDP
let udp_out_msgs = [];


let info = {
    type: null //
    ,autopilot: null //
};

// Телеметрия первого уровня (отсылается постоянно раз в секунду)
let telemetry1 = {
    time: 0
    ,b_mode: null // base mode from heartbeat
    ,c_mode: null // custom mode from heartbeat
    ,sys_status: null // system status from heartbeat
};

// Телеметрия второго уровня (отсылается раз в секунду по запросу)
let telemetry2 = {
    speed: 0
    ,lon: 0
    ,lat: 0
    ,alt: -1
    ,head: 0
    ,sys_load: -1
    ,bat_v: -1 // Battery voltage, in millivolts (1 = 1 millivolt) (Units: mV)
    ,bat_c: -1 // Battery current, in 10*milliamperes (1 = 10 milliampere), -1: autopilot does not measure the current (Units: cA)
    ,bat_rem: -1 // in % 0-100
    ,time_u: -1
    ,time_b: -1
    ,gps_fix_type: -1
    ,gps_speed: -1
    ,gps_cog: -1 // Course over ground (NOT heading, but direction of movement) in degrees * 100
    ,sats: -1
    ,press_a: -1 // Absolute pressure (hectopascal) (Units: hPa)
    ,press_d: -1 // Differential pressure 1 (hectopascal) (Units: hPa)
    ,temp: -100 //
    ,roll: 0
    ,pitch: 0
    ,yaw: 0
    ,rollspeed: 0
    ,pitchspeed: 0
    ,yawspeed: 0
    ,pos_lat: 0
    ,pos_lon: 0
    ,pos_alt: 0
    ,pos_rel_alt: 0
    ,pos_vx: 0
    ,pos_vy: 0
    ,pos_vz: 0
    ,pos_hdg: 0

    ,mav_v: null
};

const drone_commands = CommandControllers(drone_mavlink, udp_out_msgs, socket);

//
// События соединений socket.io
socket.on('connect', () => {
    console.log("Connected to socket.io server");
    io_connected = true;

    // Отправлять в браузер каждую секунду
    send_interval = setInterval(function(){
        telemetry1.time = (new Date()).getTime();
        socket.emit('telem1', telemetry1);

        if( transmit_telem2 ) socket.emit('telem2', telemetry2);

    }, 1000);

    if( info.type ){
        socket.emit('info', info);
    }

    // Отправляем информацию
    send_interval2 = setInterval(function(){
        socket.emit('info', info);
    }, 10000);

});

socket.on('disconnect', () => {
    console.log("Disconnected from socket.io server");
    io_connected = false;

    if( send_interval ) clearInterval(send_interval);
});

socket.on('error', () => {
    console.log('socket.io error');
    io_connected = false;
    if( send_interval ) clearInterval(send_interval);
});

socket.on('connect_error', () => {
    console.log('socket.io connect error');
    io_connected = false;
    if( send_interval ) clearInterval(send_interval);
});

// Запрос информации
socket.on('info', () => {
    socket.emit('info', info);
});

// При получении mavlink сообщения с сервера, отправлем его в очередь
socket.on('mav', (data) => {
    console.log('mavlink from server');
    udp_out_msgs.push(data);
});

// Команды из браузера
socket.on('command', (msg) => {
    /*
        drone_id: this.drone_id
        ,data: {
            command: command
            ,params: params
        }
     */

    console.log('cmd0');

    if( !msg || !msg.drone_id || !msg.data.command || config.DRONE_ID !== msg.drone_id ) return;

    console.log('command ' + msg.data.command);

    // Включение-выключение трансляции mavlink сообщений на сервер
    if( 'raw_mav_start' === msg.data.command ) transmit_raw_mavlink = true;
    else if( 'raw_mav_stop' === msg.data.command ) transmit_raw_mavlink = false;
    else if( 'telem2_start' === msg.data.command ) transmit_telem2 = true;
    else if( 'telem2_stop' === msg.data.command ) transmit_telem2 = false;
    else if( 'takeoff' === msg.data.command && mavlink_ready ) drone_commands.takeoff(msg.data.params);
    else if( 'switch_relay' === msg.data.command && mavlink_ready ) drone_commands.switch_relay(msg.data.params);

});


let mav_messages = 0;


//
// Разбор сообщений MAVLink
//
// https://mavlink.io/en/messages/common.html
//

drone_mavlink.on("message", function(message) {
    telemetry2.last_msg_timestamp = (new Date()).getTime();
    mav_messages++;
});

//
// После парсинга сообщений, сохраняем нужные параметры в объект, чтобы потом отправить его в браузер

// TODO 0
drone_mavlink.on("HEARTBEAT", function(message, fields) {
    /*
        type	uint8_t	Type of the MAV (quadrotor, helicopter, etc., up to 15 types, defined in MAV_TYPE ENUM) (Enum:MAV_TYPE )
        autopilot	uint8_t	Autopilot type / class. defined in MAV_AUTOPILOT ENUM (Enum:MAV_AUTOPILOT )
        base_mode	uint8_t	System mode bitfield, see MAV_MODE_FLAG ENUM in mavlink/include/mavlink_types.h (Enum:MAV_MODE_FLAG )
        custom_mode	uint32_t	A bitfield for use for autopilot-specific flags.
        system_status	uint8_t	System status flag, see MAV_STATE ENUM (Enum:MAV_STATE )
        mavlink_version	uint8_t_mavlink_version	MAVLink version, not writable by user, gets added by protocol because of magic data type: uint8_t_mavlink_version

        MAV_MODE_FLAG
            128	MAV_MODE_FLAG_SAFETY_ARMED	0b10000000 MAV safety set to armed. Motors are enabled / running / can start. Ready to fly. Additional note: this flag is to be ignore when sent in the command MAV_CMD_DO_SET_MODE and MAV_CMD_COMPONENT_ARM_DISARM shall be used instead. The flag can still be used to report the armed state.
            64	MAV_MODE_FLAG_MANUAL_INPUT_ENABLED	0b01000000 remote control input is enabled.
            32	MAV_MODE_FLAG_HIL_ENABLED	0b00100000 hardware in the loop simulation. All motors / actuators are blocked, but internal software is full operational.
            16	MAV_MODE_FLAG_STABILIZE_ENABLED	0b00010000 system stabilizes electronically its attitude (and optionally position). It needs however further control inputs to move around.
            8	MAV_MODE_FLAG_GUIDED_ENABLED	0b00001000 guided mode enabled, system flies waypoints / mission items.
            4	MAV_MODE_FLAG_AUTO_ENABLED	0b00000100 autonomous mode enabled, system finds its own goal positions. Guided flag can be set or not, depends on the actual implementation.
            2	MAV_MODE_FLAG_TEST_ENABLED	0b00000010 system has a test mode enabled. This flag is intended for temporary system tests and should not be used for stable implementations.
            1	MAV_MODE_FLAG_CUSTOM_MODE_ENABLED	0b00000001 Reserved for future use.

        MAV_STATE
            MAV_STATE_UNINIT	Uninitialized system, state is unknown.
            MAV_STATE_BOOT	System is booting up.
            MAV_STATE_CALIBRATING	System is calibrating and not flight-ready.
            MAV_STATE_STANDBY	System is grounded and on standby. It can be launched any time.
            MAV_STATE_ACTIVE	System is active and might be already airborne. Motors are engaged.
            MAV_STATE_CRITICAL	System is in a non-normal flight mode. It can however still navigate.
            MAV_STATE_EMERGENCY	System is in a non-normal flight mode. It lost control over parts or over the whole airframe. It is in mayday and going down.
            MAV_STATE_POWEROFF	System just initialized its power-down sequence, will shut down now.
            MAV_STATE_FLIGHT_TERMINATION	System is terminating itself.
     */

    info.type = fields.type;
    info.autopilot = fields.autopilot;
    telemetry1.b_mode = '';
    telemetry1.c_mode = fields.custom_mode;
    telemetry1.sys_status = fields.system_status;
    telemetry2.mav_v = fields.mavlink_version;

    if( 1 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_CUSTOM_MODE_ENABLED ';
    if( 2 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_TEST_ENABLED ';
    if( 4 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_AUTO_ENABLED ';
    if( 8 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_GUIDED_ENABLED ';
    if( 16 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_STABILIZE_ENABLED ';
    if( 32 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_HIL_ENABLED ';
    if( 64 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_MANUAL_INPUT_ENABLED ';
    if( 128 & fields.base_mode ) telemetry1.b_mode = telemetry1.b_mode + 'MAV_MODE_FLAG_SAFETY_ARMED ';

});

// 1
drone_mavlink.on("SYS_STATUS", function(message, fields) {
    telemetry2.sys_load = Math.round(fields.load/10);
    telemetry2.bat_v = fields.voltage_battery / 1000;
    telemetry2.bat_c = fields.current_battery;
    telemetry2.bat_rem = fields.battery_remaining;

    /*
        onboard_control_sensors_present	uint32_t	Bitmask showing which onboard controllers and sensors are present. Value of 0: not present. Value of 1: present. Indices defined by ENUM MAV_SYS_STATUS_SENSOR (Enum:MAV_SYS_STATUS_SENSOR )
        onboard_control_sensors_enabled	uint32_t	Bitmask showing which onboard controllers and sensors are enabled: Value of 0: not enabled. Value of 1: enabled. Indices defined by ENUM MAV_SYS_STATUS_SENSOR (Enum:MAV_SYS_STATUS_SENSOR )
        onboard_control_sensors_health	uint32_t	Bitmask showing which onboard controllers and sensors are operational or have an error: Value of 0: not enabled. Value of 1: enabled. Indices defined by ENUM MAV_SYS_STATUS_SENSOR (Enum:MAV_SYS_STATUS_SENSOR )
        load	uint16_t	Maximum usage in percent of the mainloop time, (0%: 0, 100%: 1000) should be always below 1000 (Units: d%)
        voltage_battery	uint16_t	Battery voltage, in millivolts (1 = 1 millivolt) (Units: mV)
        current_battery	int16_t	Battery current, in 10*milliamperes (1 = 10 milliampere), -1: autopilot does not measure the current (Units: cA)
        battery_remaining	int8_t	Remaining battery energy: (0%: 0, 100%: 100), -1: autopilot estimate the remaining battery (Units: %)
        drop_rate_comm	uint16_t	Communication drops in percent, (0%: 0, 100%: 10'000), (UART, I2C, SPI, CAN), dropped packets on all links (packets that were corrupted on reception on the MAV) (Units: c%)
        errors_comm	uint16_t	Communication errors (UART, I2C, SPI, CAN), dropped packets on all links (packets that were corrupted on reception on the MAV)
        errors_count1	uint16_t	Autopilot-specific errors
        errors_count2	uint16_t	Autopilot-specific errors
        errors_count3	uint16_t	Autopilot-specific errors
        errors_count4	uint16_t	Autopilot-specific errors
     */

});

// 2
drone_mavlink.on("SYSTEM_TIME", function(message, fields) {
    telemetry2.time_u = fields.time_unix_usec;
    telemetry2.time_b = Math.round(fields.time_boot_ms/1000);
});

// TODO 22
drone_mavlink.on("PARAM_VALUE", function(message, fields) {

    /*
    param_id	char[16]	Onboard parameter id, terminated by NULL if the length is less than 16
    human-readable chars and WITHOUT null termination (NULL) byte if the length is exactly 16 chars -
    applications have to provide 16+1 bytes storage if the ID is stored as string

    param_value	float	Onboard parameter value
    param_type	uint8_t	Onboard parameter type: see the MAV_PARAM_TYPE enum for supported data types. (Enum:MAV_PARAM_TYPE )
    param_count	uint16_t	Total number of onboard parameters
    param_index	uint16_t	Index of this onboard parameter

    1	MAV_PARAM_TYPE_UINT8	8-bit unsigned integer
    2	MAV_PARAM_TYPE_INT8	8-bit signed integer
    3	MAV_PARAM_TYPE_UINT16	16-bit unsigned integer
    4	MAV_PARAM_TYPE_INT16	16-bit signed integer
    5	MAV_PARAM_TYPE_UINT32	32-bit unsigned integer
    6	MAV_PARAM_TYPE_INT32	32-bit signed integer
    7	MAV_PARAM_TYPE_UINT64	64-bit unsigned integer
    8	MAV_PARAM_TYPE_INT64	64-bit signed integer
    9	MAV_PARAM_TYPE_REAL32	32-bit floating-point
    10	MAV_PARAM_TYPE_REAL64	64-bit floating-point
     */

});

// 24
drone_mavlink.on("GPS_RAW_INT", function(message, fields) {
    telemetry2.gps_fix_type = fields.fix_type;
    telemetry2.lat = fields.lat/10000000;
    telemetry2.lon = fields.lon/10000000;
    telemetry2.alt = fields.alt/1000;
    telemetry2.gps_speed = fields.vel/100; // GPS ground speed (m/s * 100). If unknown, set to: UINT16_MAX (Units: cm/s)
    telemetry2.gps_cog = fields.cog;
    telemetry2.sats = fields.satellites_visible;
});

// TODO 25
drone_mavlink.on("GPS_STATUS", function(message, fields) {

});

// TODO 26
drone_mavlink.on("SCALED_IMU", function(message, fields) {

});

// TODO 27
drone_mavlink.on("RAW_IMU", function(message, fields) {

});

// 29
drone_mavlink.on("SCALED_PRESSURE", function(message, fields) {
    telemetry2.press_a = Math.round(fields.press_abs);
    telemetry2.press_d = fields.press_diff;
    telemetry2.temp = Math.round(fields.temperature/100); // Temperature measurement (0.01 degrees celsius) (Units: cdegC)
});

// 30
drone_mavlink.on("ATTITUDE", function(message, fields) {
    const pi = Math.PI;
    telemetry2.roll = Math.round(fields.roll * (180/pi)); // Roll angle (rad, -pi..+pi) (Units: rad)
    telemetry2.pitch = Math.round(fields.pitch * (180/pi));
    telemetry2.yaw = Math.round(fields.yaw * (180/pi));
    telemetry2.rollspeed = Math.round(fields.rollspeed * (180/pi)); // Roll angular speed (rad/s) (Units: rad/s)
    telemetry2.pitchspeed = Math.round(fields.pitchspeed * (180/pi));
    telemetry2.yawspeed = Math.round(fields.yawspeed * (180/pi));
});

// TODO 32
drone_mavlink.on("LOCAL_POSITION_NED", function(message, fields) {

});

// 33
drone_mavlink.on("GLOBAL_POSITION_INT", function(message, fields) {
    telemetry2.pos_lat = fields.lat/10000000; // Latitude, expressed as degrees * 1E7 (Units: degE7)
    telemetry2.pos_lon = fields.lon/10000000;
    telemetry2.pos_alt = fields.alt/1000; // Altitude in meters, expressed as * 1000 (millimeters), AMSL (not WGS84 - note that virtually all GPS modules provide the AMSL as well) (Units: mm)
    telemetry2.pos_rel_alt = fields.relative_alt/1000; // Altitude above ground in meters, expressed as * 1000 (millimeters) (Units: mm)
    telemetry2.pos_vx = fields.vx/100; // Ground X Speed (Latitude, positive north), expressed as m/s * 100 (Units: cm/s)
    telemetry2.pos_vy = fields.vy/100;
    telemetry2.pos_vz = fields.vz/100;
    telemetry2.pos_hdg = Math.round(fields.hdg/100); // Vehicle heading (yaw angle) in degrees * 100, 0.0..359.99 degrees. If unknown, set to: UINT16_MAX (Units: cdeg)
});

// TODO 35
drone_mavlink.on("RC_CHANNELS_RAW", function(message, fields) {

});

// TODO 39
drone_mavlink.on("MISSION_ITEM", function(message, fields) {

});

// TODO 42
drone_mavlink.on("MISSION_CURRENT", function(message, fields) {

});

// TODO 44
drone_mavlink.on("MISSION_COUNT", function(message, fields) {

});

// TODO 46
drone_mavlink.on("MISSION_ITEM_REACHED", function(message, fields) {
    /*
        A certain mission item has been reached.
        The system will either hold this position (or circle on the orbit) or
        (if the autocontinue on the WP was set) continue to the next waypoint.

        seq	uint16_t	Sequence
     */
});

// TODO 47
drone_mavlink.on("MISSION_ACK", function(message, fields) {
    /*
        Ack message during waypoint handling. The type field states if this message is a positive ack (type=0)
        or if an error happened (type=non-zero).

        target_system	uint8_t	System ID
        target_component	uint8_t	Component ID
        type	uint8_t	See MAV_MISSION_RESULT enum (Enum:MAV_MISSION_RESULT )
        mission_type **	uint8_t	Mission type, see MAV_MISSION_TYPE (Enum:MAV_MISSION_TYPE )
     */

});

// TODO !! 62
drone_mavlink.on("NAV_CONTROLLER_OUTPUT", function(message, fields) {
    /*
    nav_roll	float	Current desired roll in degrees (Units: deg)
    nav_pitch	float	Current desired pitch in degrees (Units: deg)
    nav_bearing	int16_t	Current desired heading in degrees (Units: deg)
    target_bearing	int16_t	Bearing to current waypoint/target in degrees (Units: deg)
    wp_dist	uint16_t	Distance to active waypoint in meters (Units: m)
    alt_error	float	Current altitude error in meters (Units: m)
    aspd_error	float	Current airspeed error in meters/second (Units: m/s)
    xtrack_error	float	Current crosstrack error on x-y plane in meters (Units: m)
     */
});

// TODO 73
drone_mavlink.on("MISSION_ITEM_INT", function(message, fields) {
    /*
        Message encoding a mission item. This message is emitted to announce the presence of a mission item and to set a mission item on the system. The mission item can be either in x, y, z meters (type: LOCAL) or x:lat, y:lon, z:altitude. Local frame is Z-down, right handed (NED), global frame is Z-up, right handed (ENU). See alsohttp://qgroundcontrol.org/mavlink/waypoint_protocol.+

            Field Name	Type	Description
            target_system	uint8_t	System ID
            target_component	uint8_t	Component ID
            seq	uint16_t	Waypoint ID (sequence number). Starts at zero. Increases monotonically for each waypoint, no gaps in the sequence (0,1,2,3,4).
            frame	uint8_t	The coordinate system of the waypoint. see MAV_FRAME in mavlink_types.h (Enum:MAV_FRAME )
            command	uint16_t	The scheduled action for the waypoint. see MAV_CMD in common.xml MAVLink specs (Enum:MAV_CMD )
            current	uint8_t	false:0, true:1
            autocontinue	uint8_t	autocontinue to next wp
            param1	float	PARAM1, see MAV_CMD enum
            param2	float	PARAM2, see MAV_CMD enum
            param3	float	PARAM3, see MAV_CMD enum
            param4	float	PARAM4, see MAV_CMD enum
            x	int32_t	PARAM5 / local: x position in meters * 1e4, global: latitude in degrees * 10^7
            y	int32_t	PARAM6 / y position: local: x position in meters * 1e4, global: longitude in degrees *10^7
            z	float	PARAM7 / z position: global: altitude in meters (relative or absolute, depending on frame.
            mission_type **	uint8_t	Mission type, see MAV_MISSION_TYPE (Enum:MAV_MISSION_TYPE )

     */
});

// TODO 74
drone_mavlink.on("VFR_HUD", function(message, fields) {
    /*
        Metrics typically displayed on a HUD for fixed wing aircraft
            Field Name	Type	Description
            airspeed	float	Current airspeed in m/s (Units: m/s)
            groundspeed	float	Current ground speed in m/s (Units: m/s)
            heading	int16_t	Current heading in degrees, in compass units (0..360, 0=north) (Units: deg)
            throttle	uint16_t	Current throttle setting in integer percent, 0 to 100 (Units: %)
            alt	float	Current altitude (MSL), in meters (Units: m)
            climb	float	Current climb rate in meters/second (Units: m/s)
     */
});

// TODO !! 77
drone_mavlink.on("COMMAND_ACK", function(message, fields) {
    /*
    command	uint16_t	Command ID, as defined by MAV_CMD enum. (Enum:MAV_CMD )
    result	uint8_t	See MAV_RESULT enum (Enum:MAV_RESULT )
    progress **	uint8_t	WIP: Also used as result_param1, it can be set with a enum containing the errors reasons of why the command was denied or the progress percentage or 255 if unknown the progress when result is MAV_RESULT_IN_PROGRESS.
    result_param2 **	int32_t	WIP: Additional parameter of the result, example: which parameter of MAV_CMD_NAV_WAYPOINT caused it to be denied.
    target_system **	uint8_t	WIP: System which requested the command to be executed
    target_component **	uint8_t	WIP: Component which requested the command to be executed

    ** MAV_CMD


    ** MAV_RESULT


     */

    console.log('77 COMMAND_ACK   com:' + fields.command + ', res: ' + fields.result);

    // Отправляем в GCS подтверждение исполнения команды и ее результат
    if( io_connected ) socket.emit('com_ack', fields);

});

// TODO !! 125
drone_mavlink.on("POWER_STATUS", function(message, fields) {
    /*
    Vcc	uint16_t	5V rail voltage in millivolts (Units: mV)
    Vservo	uint16_t	servo rail voltage in millivolts (Units: mV)
    flags	uint16_t	power supply status flags (see MAV_POWER_STATUS enum) (Enum:MAV_POWER_STATUS )
     */
});

// TODO 141
drone_mavlink.on("ALTITUDE", function(message, fields) {

});

// TODO 148
drone_mavlink.on("AUTOPILOT_VERSION", function(message, fields) {

});

// TODO 149
drone_mavlink.on("LANDING_TARGET", function(message, fields) {

});

// TODO 152
drone_mavlink.on("MEMINFO", function(message, fields) {

});

// TODO 163
drone_mavlink.on("AHRS", function(message, fields) {

});

// TODO 165
drone_mavlink.on("HWSTATUS", function(message, fields) {

});

// TODO 241
drone_mavlink.on("VIBRATION", function(message, fields) {
    /*
    time_usec	uint64_t	Timestamp (micros since boot or Unix epoch) (Units: us)
    vibration_x	float	Vibration levels on X-axis
    vibration_y	float	Vibration levels on Y-axis
    vibration_z	float	Vibration levels on Z-axis
    clipping_0	uint32_t	first accelerometer clipping count
    clipping_1	uint32_t	second accelerometer clipping count
    clipping_2	uint32_t	third accelerometer clipping count
     */
});

// TODO !! 242
drone_mavlink.on("HOME_POSITION", function(message, fields) {
    /*
    This message can be requested by sending the MAV_CMD_GET_HOME_POSITION command.
    The position the system will return to and land on. The position is set automatically by
    the system during the takeoff in case it was not explicitely set by the operator before or after.
    The position the system will return to and land on. The global and local positions encode
    the position in the respective coordinate frames, while the q parameter encodes the orientation of
    the surface. Under normal conditions it describes the heading and terrain slope, which can be used
    by the aircraft to adjust the approach. The approach 3D vector describes the point to which the system
    should fly in normal flight mode and then perform a landing sequence along the vector.

    latitude	int32_t	Latitude (WGS84), in degrees * 1E7 (Units: degE7)
    longitude	int32_t	Longitude (WGS84, in degrees * 1E7 (Units: degE7)
    altitude	int32_t	Altitude (AMSL), in meters * 1000 (positive for up) (Units: mm)
    x	float	Local X position of this position in the local coordinate frame (Units: m)
    y	float	Local Y position of this position in the local coordinate frame (Units: m)
    z	float	Local Z position of this position in the local coordinate frame (Units: m)
    q	float[4]	World to surface normal and heading transformation of the takeoff position. Used to indicate the heading and slope of the ground
    approach_x	float	Local X position of the end of the approach vector. Multicopters should set this position based on their takeoff path. Grass-landing fixed wing aircraft should set it the same way as multicopters. Runway-landing fixed wing aircraft should set it to the opposite direction of the takeoff, assuming the takeoff happened from the threshold / touchdown zone. (Units: m)
    approach_y	float	Local Y position of the end of the approach vector. Multicopters should set this position based on their takeoff path. Grass-landing fixed wing aircraft should set it the same way as multicopters. Runway-landing fixed wing aircraft should set it to the opposite direction of the takeoff, assuming the takeoff happened from the threshold / touchdown zone. (Units: m)
    approach_z	float	Local Z position of the end of the approach vector. Multicopters should set this position based on their takeoff path. Grass-landing fixed wing aircraft should set it the same way as multicopters. Runway-landing fixed wing aircraft should set it to the opposite direction of the takeoff, assuming the takeoff happened from the threshold / touchdown zone. (Units: m)
    time_usec **	uint64_t	Timestamp (microseconds since UNIX epoch or microseconds since system boot) (Units: us)
     */

});

// TODO 244
drone_mavlink.on("MESSAGE_INTERVAL", function(message, fields) {
    /*
        message_id	uint16_t	The ID of the requested MAVLink message. v1.0 is limited to 254 messages.
        interval_us	int32_t	The interval between two messages, in microseconds. A value of -1 indicates this stream is disabled, 0 indicates it is not available, > 0 indicates the interval at which it is sent. (Units: us)
     */
});

// TODO !! 253
drone_mavlink.on("STATUSTEXT", function(message, fields) {
    /*
    severity	uint8_t	Severity of status. Relies on the definitions within RFC-5424. See enum MAV_SEVERITY. (Enum:MAV_SEVERITY )
    text	char[50]	Status text message, without null termination character

        0	MAV_SEVERITY_EMERGENCY	System is unusable. This is a "panic" condition.
        1	MAV_SEVERITY_ALERT	Action should be taken immediately. Indicates error in non-critical systems.
        2	MAV_SEVERITY_CRITICAL	Action must be taken immediately. Indicates failure in a primary system.
        3	MAV_SEVERITY_ERROR	Indicates an error in secondary/redundant systems.
        4	MAV_SEVERITY_WARNING	Indicates about a possible future error if this is not resolved within a given timeframe. Example would be a low battery warning.
        5	MAV_SEVERITY_NOTICE	An unusual event has occured, though not an error condition. This should be investigated for the root cause.
        6	MAV_SEVERITY_INFO	Normal operational messages. Useful for logging. No action is required for these messages.
        7	MAV_SEVERITY_DEBUG	Useful non-operational messages that can assist in debugging. These should not occur during normal operation.

     */

    if( io_connected ) socket.emit('status_text', fields);

});





//
// После загрузки и парсинга XML файлов, модуль mavlink готов к приему сообщений
drone_mavlink.on("ready", function() {
    console.log('mavlink ready');

    mavlink_ready = true;

    //
    // Запускаем UDP сервер
    udp_server.bind(config.MAVLINK_UDP_PORT, config.MAVLINK_UDP_HOST);

    udp_server.on('listening', function () {
        const address = udp_server.address();
        console.log('UDP server on ' + address.address + ":" + address.port);
    });

    udp_server.on('close', function () {
        console.log('UDP closed');
    });

    // Когда приходит mavlink сообщение от автопилота по UDP
    udp_server.on('message', function (message, remote) {

        // Исходящие сообщения для UDP
        let msg_to_board = udp_out_msgs.shift();
        if( msg_to_board ){
            udp_server.send(msg_to_board, 0, msg_to_board.length, remote.port, remote.address, function(err){
                if( err ) console.log('UDP send error ' + err);
            });
        }

        // Перенаправляем его на сервер, если есть соединение и флаг
        if( io_connected && transmit_raw_mavlink ){
            socket.emit('mav', message);
        }

        // Парсим сообщение
        drone_mavlink.parse(message);

    });


});


// Пишем в консоль сколько сообщений получено
setInterval(function(){
    console.log('received msgs for last 10sec: ' + mav_messages);
    mav_messages = 0;
}, 10000);


/*

// Отправка исходящего сообщения на борт
    const send_to_board = function(message){
        io_client.emit('fromserver', message.buffer);
    };
    // Создаем исходящее сообщение и отправляем его на борт
    drone_mavlink.createMessage(com_data.command, com_data.params, send_to_board);




    // Первое сообщение - запрос всех параметров
    drone_mavlink.createMessage("PARAM_REQUEST_LIST", {
        'target_system': drone.sys_id
        ,'target_component': drone.comp_id
    }, send_to_board);


 */





