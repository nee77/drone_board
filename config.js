module.exports = {

    // socket.io сервер
    MAV_IO_SERVER: 'http://192.168.1.34:8091'
    ,DRONE_ID: 'drone_id_1234567890'

    // UDP хост и порт куда приходят сообщения mavlink
    , MAVLINK_UDP_HOST: '127.0.0.1'
    , MAVLINK_UDP_PORT: 15001

    // параметры MAVLink
    , BOARD_SYS_ID: 1
    , BOARD_COMP_ID: 1
    , MAVLINK_VERSION: 'v1.0'
    , MAVLINK_MSG_DEF: ["common", "ardupilotmega"]
};