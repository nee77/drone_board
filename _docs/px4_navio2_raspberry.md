# Установка образа ОС на SD карту

Emlid Raspbian Strech https://docs.emlid.com/navio2/common/ardupilot/configuring-raspberry-pi/
Etcher https://etcher.io/


## Настройка подключения к WiFi

Файл в корне SD-карты wpa_supplicant.conf

    network={
      ssid="название_wifi_сети"
      psk="пароль_wifi_сети"
    }

## Обновление и начальная конфигурация дистрибутива

    ssh pi@IP_ADDRESS

Обновляем дистрибутив:

    sudo apt-get update && sudo apt-get dist-upgrade -y

Расширяем файловую систему на весь объем SD-карты:

    sudo raspi-config --expand-rootfs

и перегружаем 

    sudo reboot

Расширенные настройки http://docs.px4.io/en/flight_controller/raspberry_pi_navio2.html


## Отключение светодиода

Редактировать файл

    sudo nano /boot/config.txt

Закомментировать строку

    #dtoverlay=navio-rgb    



# Установка PX4

    sudo apt-get update
    sudo apt-get install cmake python-empy

    git clone https://github.com/roboflot-ru/Firmware.git
    
    cd Firmware
    make posix_rpi_native # for native build
    
The "px4" executable file is in the directory build/posix_rpi_native/. Run it directly with:
    
    sudo ./build/posix_rpi_native/px4 ./posix-configs/rpi/px4.config





    


    sudo usermod -a -G dialout $USER

Logout and login again

    sudo apt-get remove modemmanager

    curl https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_common_deps.sh
    source ubuntu_sim_common_deps.sh

    git clone https://github.com/raspberrypi/tools.git ${HOME}/rpi-tools

test compiler

    $HOME/rpi-tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc -v
    
PATH variable

    echo 'export PATH=$PATH:$HOME/rpi-tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin' >> ~/.profile
    
PATH variable only for this session

    export PATH=$PATH:$HOME/rpi-tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin



    